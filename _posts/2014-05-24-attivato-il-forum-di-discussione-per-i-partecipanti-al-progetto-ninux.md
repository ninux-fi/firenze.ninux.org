---
layout: post
title: !binary |-
  QXR0aXZhdG8gaWwgZm9ydW0gZGkgZGlzY3Vzc2lvbmUgcGVyIGkgcGFydGVj
  aXBhbnRpIGFsIHByb2dldHRvIE5pbnV4
created: 1400968495
---
Dopo una <a href="http://www.mail-archive.com/wireless@ml.ninux.org/msg13696.html">lunga discussione</a> nata all'interno della <a href="http://www.mail-archive.com/wireless-ml@ninux.org/">mailing list "wireless"</a> è stata decisa l'attivazione di un servizio di forum indirizzato specialmente ai nuovi arrivati, aumentati considerevolmente negli ultimi giorni.

Il servizio è raggiungibile attualmente all'indirizzo http://10.135.6.99/ da dentro ninux oppure a http://nazza.servebeer.com/ da internet.

Fra le varie sezioni ce n'è ovviamente una dedicata alla nostra isola: http://nazza.servebeer.com/viewforum.php?f=7&sid=aafe40bd557b4e17ab8f65f45fc90f58

Il servizio è ancora sperimentale e quindi da perfezionare ma invitiamo tutti a provarlo così da migliorarlo.
