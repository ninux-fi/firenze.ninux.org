---
layout: page
title: Cosa facciamo
permalink: /cosa-facciamo/
drupal_node: 12
---



La nostra idea è di creare una rete alternativa e gestita direttamente dagli utenti senza intermediari.
Il nostro sogno è di riuscire a coprire tutto il territorio nazionale e magari un giorno unirci alle varie reti libere degli altri stati europei.

Questa è la mappa dei link attivi sul territorio fiorentino [mappa](http://map.ninux.org/select/bellanzer2/) (eventualmente ingrandire la mappa per vedere tutta la città).

Alcune spiegazioni:

* i punti verdi sono i nodi attivi, cioè quelli che già fanno parte della rete;
* le righe verdi sono i link attivi
* i punti marroni sono i nodi potenziali, cioè quelli che si sono iscritti ma che ancora non hanno nessun link attivo
